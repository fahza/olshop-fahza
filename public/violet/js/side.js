$(document).ready(function(){

   domain = "http://localhost:8000/"
   // $('.single-product-item').click(function(){
   //    let name= $(this).data("nama")
   //    let harga=$(this).data("harga")
   //    let picture=$(this).find("img").attr("src")

   //    let html = $(".content").prepend("<img src='"+picture+"' class='picture rounded mx-auto d-block'>"+
   //    "<h5 class='name text-center mt-2'>"+name+"</h5>"+
   //    "<h4 class='price text-center mt2'>"+harga+"</h4>"+
   //    "<form class='qty d-flex justify-content-center'>"+
   //       "<span><a href=''>+</a></span>"+
   //       "<div class='form-group'>"+
   //       "<input type='text' class='form-control'   placeholder='Qty'>"+
   //       "</div>"+
   //       "<span><a href=''>-</a></span>"+
   //       "<button class='delete btn btn-danger'>Delete</button>"+
   //    "</form>")
   //    $(".name").append(name)
   //    $(".price").append(harga)
   // })
   refresh() ; 
   //get data / Refresh sidebar 
   function refresh() 
   {
      $('.isi').html("");
      $.getJSON("http://localhost:8000/cart", function(data)
      {
         //insert
         data.list.forEach(myFuction);
         function myFuction(item, index) {
            let copy = $('.content:first').clone(true).show()
            copy.find('.name').html(item.nama)
            copy.find('.price').html(item.harga)
            copy.find('.qty').val(item.jmlh)
            copy.find('.qtyEdit').attr("id", item.id)
            copy.find('img').attr('src',domain+"violet/img/products/"+item.gambar)
            $('.isi').prepend(copy)
         }
      })
   }

   $('.single-product-item').click(function(){
      $('.sidebar').slideDown()
      let id = $(this).data("id")

      let test = $.post("http://localhost:8000/cart",{"_token": $('meta[name="csrf-token"]').attr('content') ,"product":id}, function(jsonHtml){
         refresh()
      })
      // let copy = $('.content:first').clone(true).show()

      // copy.find('.name').html(name)
      // copy.find('.price').html(harga)
      // copy.find('.picture').attr("src",picture)
      // copy.find('.delete').click(function(){
      //    $(this).parent().parent().hide("slow", function(){
      //       $(this).remove()
      //    })
      // })

      // $('.sidebar').prepend(copy)
   });

   $('.delete').click(function(){
      let del = $(this).parent().attr("id")

      let test = $.post("http://localhost:8000/cart/delete/id", {"_token": $('meta[name="csrf-token"]').attr('content') ,"delete":del})
      refresh()
   })

   $('.plus').click(function(){
      let tetes = $(this).parent().parent().attr("id")
      let plus = $(this).attr("class")
      $.post("http://localhost:8000/cart/plusMin/id",{"_token": $('meta[name="csrf-token"]').attr('content') ,"id":tetes, "tmbh":plus})
      refresh()
   })

   $('.min').click(function(){
      let tetes = $(this).parent().parent().attr("id")
      let min = $(this).attr("class")
      $.post("http://localhost:8000/cart/plusMin/id",{"_token": $('meta[name="csrf-token"]').attr('content') ,"id":tetes, "min":min})
      refresh()
   })
   
   $('.close').click(function(){
      $('.sidebar').slideUp()
   })

   $('.showSide').click(function(){
      $('.sidebar').slideToggle()
   })

   // order


   $('.checkOut').click(function(){
      $('.content-summary').html("")
      $.getJSON("http://localhost:8000/cart",function(data){
         let total = 0 ;
         user = data.user_id
         data.list.forEach(myFuction)
         function myFuction(item, index){
            let copy = $(".summary-item:first").clone(true).show()
            copy.find('.nama').html(item.nama)
            copy.find('.qty').html(item.jmlh + 'x')
            copy.find('.qty').attr('data-qty',item.jmlh)
            copy.find('.harga').html('Rp '+item.harga)
            copy.find('.harga').attr('data-harga',item.harga)
            copy.find('.harga').attr('data-subtotal', item.harga*item.jmlh)
            $('.content-summary').prepend(copy)
            total = total + (item.harga*item.jmlh);
            
         }
         $('.total-price').html('Rp '+total)
         $('.total-price').data('total', total)
      })
      $('.sidebar').slideUp()
      $('.pop').fadeIn()
   })

   $('.sendToOrder').click(function(){
      let namaPenerima = $('#name').val()
      let alamat = $('#address').val()
      let contact = $('#contact').val()
      let totalHarga = $('.total-price').data('total')
      console.log(totalHarga)
      data  = {
                  "_token": $('meta[name="csrf-token"]').attr('content'),
                  "namaPenerima":namaPenerima,
                  "alamat":alamat,
                  "contact": contact, 
                  "total":totalHarga , 
                  "id": user
               };
      $.post(domain+"dashboard/order",data,function(response)
      {
         //aksi setelah berhasil
         console.log(response) ; 
         alert("berhasil") ;
      });
   })

   $('.popClose').click(function(){
      $('.pop').fadeOut()
   })
})

