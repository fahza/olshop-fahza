<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Yoga Studio Template">
    <meta name="keywords" content="Yoga, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>@yield('title')</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <!-- contoh -->
    <!-- {{ asset('awesome/css/all.css') }} -->
    <link rel="stylesheet" href="{{ asset('violet/css/bootstrap.min.css')}}" >
    <link rel="stylesheet" href="{{ asset('violet/css/font-awesome.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/nice-select.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/owl.carousel.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/magnific-popup.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/slicknav.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/style.css') }}" >
    <link rel="stylesheet" href="{{ asset('violet/css/side.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    
    <!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
    <!-- Search model end -->
    <!-- Order Popup Begin -->
    <div class="summary-item pt-2" style="display:none">
        <span class="nama">Baju</span>
        <span class="qty" data-qty="">15x</span>
        <span class="harga" data-harga="">Rp 300000</span>
    </div>
    <div class="pop justify-content-center" style="display:none">
        <div class="popContent">
            <button class="popClose btn">x</button>
            <form class="popup">
                <div class="row">
                    <div class="col-lg-12 resize">
                        <h3>Please Complete This Form</h3>
                    </div>
                    <div class="col-lg-9">
                        <div class="row pt-4 pb-3">
                            <label for="name" class="col-sm-2 col-form-label">Name*</label>
                            <div class="col-lg-10">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="row py-3">
                            <label for="address" class="col-sm-2 col-form-label">Address*</label>
                            <div class="col-lg-10">
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                            </div>
                        </div>
                        <div class="row pt-3 pb-5">
                            <label for="contact" class="col-sm-2 col-form-label">Contact*</label>
                            <div class="col-lg-10">
                            <input type="text" class="form-control" name="contact" id="contact" placeholder="Contact">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 sumsum">
                        <div class="summary">
                            <p>Summary</p>
                            <div class="content-summary">
                            </div>
                        </div>
                        <div class="total-price">
                            <span>Rp 1000000</span>
                        </div>
                    </div>
                </div>
                <button class="btn btn-outline-info sendToOrder" type="button">Place Your Order</button>
            </form>
        </div>
    </div>
    <!-- Order Popup End -->

    <!-- Sidebar Begin -->
    <div class="content" style="display:none">
        <img src="" alt="" class="picture rounded mx-auto d-block">
        <h5 class="name text-center mt-2"></h5>
        <h4 class="price text-center mt2"></h4>
        <form class="qtyEdit d-flex justify-content-center">
            <span><a class="plus" id="">+</a></span>
            <div class="form-group">
                <input type="text" class="form-control qty" placeholder="Qty">
            </div>
            <span><a class="min" id="">-</a></span>
            <a class="delete btn btn-danger">X</a>
        </form>
    </div>
    
    <div class="sidebar" style="display:none">
      <button class="close btn">X</button>
      <div class="isi"></div>
      <div class="button d-flex justify-content-center">
        <button type="button" class="btn btn-light">Add To Cart</button>
        <button type="button" class="btn btn-light checkOut">Check Out</button>
      </div>
   </div>
    <!-- Sidebar End -->
    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="/"><img src="{{asset('violet/img/logo.png')}}"></a>
                </div>
                <div class="header-right test">
                    <img src="{{asset('violet/img/icons/search.png')}}" alt="" class="search-trigger">
                    <img src="{{asset('violet/img/icons/man.png')}}" alt="">    
                    <a href="#" class="showSide">
                        <img src="{{asset('violet/img/icons/bag.png')}}" alt="">
                    </a>
                </div>
                <div class="user-access">
                    <a href="#">Register</a>
                    <a href="#" class="in">Sign in</a>
                </div>
                <nav class="main-menu mobile-menu">
                    <ul>
                        <li><a class="active" href="/">Home</a></li>
                        <li><a href="/product">Shop</a>
                            <ul class="sub-menu">
                                <li><a href="/product/1">Product Page</a></li>
                                <li><a href="/chart">Shopping Card</a></li>
                                <li><a href="/check">Check out</a></li>
                            </ul>
                        </li>
                        <li><a href="/product/1">About</a></li>
                        <li><a href="/check">Blog</a></li>
                        <li><a href="/contact">Contact</a></li>
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else
                        <li><a>{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header Info Begin -->
    <div class="header-info">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="header-item">
                        <img src="{{asset('violet/img/icons/delivery.png')}}" alt="">
                        <p>Free shipping on orders over $30 in USA</p>
                    </div>
                </div>
                <div class="col-md-4 text-left text-lg-center">
                    <div class="header-item">
                        <img src="{{asset('violet/img/icons/voucher.png')}}" alt="">
                        <p>20% Student Discount</p>
                    </div>
                </div>
                <div class="col-md-4 text-left text-xl-right">
                    <div class="header-item">
                    <img src="{{asset('violet/img/icons/sales.png')}}" alt="">
                    <p>30% off on dresses. Use code: 30OFF</p>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Info End -->
    <!-- Header End -->

    <!-- Content -->
    @yield('content')
    <!-- Content End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section spad">
        <div class="container">
            <div class="newslatter-form">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="#">
                            <input type="text" placeholder="Your email address">
                            <button type="submit">Subscribe to our newsletter</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer-widget">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h4>About us</h4>
                            <ul>
                                <li>About Us</li>
                                <li>Community</li>
                                <li>Jobs</li>
                                <li>Shipping</li>
                                <li>Contact Us</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h4>Customer Care</h4>
                            <ul>
                                <li>Search</li>
                                <li>Privacy Policy</li>
                                <li>2019 Lookbook</li>
                                <li>Shipping & Delivery</li>
                                <li>Gallery</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h4>Our Services</h4>
                            <ul>
                                <li>Free Shipping</li>
                                <li>Free Returnes</li>
                                <li>Our Franchising</li>
                                <li>Terms and conditions</li>
                                <li>Privacy Policy</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h4>Information</h4>
                            <ul>
                                <li>Payment methods</li>
                                <li>Times and shipping costs</li>
                                <li>Product Returns</li>
                                <li>Shipping methods</li>
                                <li>Conformity of the products</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-links-warp">
			<div class="container">
				<div class="social-links">
					<a href="" class="instagram"><i class="fa fa-instagram"></i><span>instagram</span></a>
					<a href="" class="pinterest"><i class="fa fa-pinterest"></i><span>pinterest</span></a>
					<a href="" class="facebook"><i class="fa fa-facebook"></i><span>facebook</span></a>
					<a href="" class="twitter"><i class="fa fa-twitter"></i><span>twitter</span></a>
					<a href="" class="youtube"><i class="fa fa-youtube"></i><span>youtube</span></a>
					<a href="" class="tumblr"><i class="fa fa-tumblr-square"></i><span>tumblr</span></a>
				</div>
			</div>

<div class="container text-center pt-5">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>


		</div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('violet/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('violet/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('violet/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('violet/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('violet/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('violet/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('violet/js/mixitup.min.js')}}"></script>
    <script src="{{asset('violet/js/main.js')}}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('violet/js/side.js') }}"></script>
    <!-- <script>
        $(document).ready(function(){
            refresh()
        })
        
    </script> -->
</body>

</html>