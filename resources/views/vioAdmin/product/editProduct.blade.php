@extends('../vioAdmin/template/mainAdmin')

@section('title','Edit Product')

@section('content')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<!-- INPUTS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Edit Product</h3>
						</div>
						<div class="panel-body">
							<form action="" method="POST">
								@csrf 

								<select name="categoryid" class="form-control @error('categoryid') is-invalid invalid @enderror">

									@foreach($categoryID as $cat)
										<option value="{{$cat->id}}" @if($cat->id == old('categoryid' , $product->category_id)) selected @endif>{{$cat->categori}}</option>
									@endforeach
								
								</select>
								
								@error('categoryid')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="text" class="form-control @error('name') invalid is-invalid @enderror" name="name" placeholder="Name" value="@if(old('name')) {{old('name')}} @else {{$product->nama}} @endif">
								@error('name')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="text" class="form-control @error('warna') invalid is-invalid @enderror" name="warna" placeholder="Color" value="@if(old('warna')) {{old('warna')}} @else {{$product->warna}} @endif">
								@error('warna')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="text" class="form-control @error('ukuran') invalid is-invalid @enderror" name="ukuran" placeholder="Size" value="@if(old('ukuran')) {{old('ukuran')}} @else {{$product->ukuran}} @endif">
								@error('ukuran')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="text" class="form-control @error('stock') invalid is-invalid @enderror" name="stock" placeholder="Stock" value="@if(old('stock')) {{old('stock')}} @else {{$product->stok}} @endif">
								@error('stock')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="text" class="form-control @error('price') invalid is-invalid @enderror" name="price" placeholder="Price" value="@if(old('price')) {{old('price')}} @else {{$product->harga}} @endif">
								@error('price')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror

								<input type="file" class="form-control @error('picture') invalid is-invalid @enderror" name="picture" placeholder="Picture" value="@if(old('picture')) {{old('picture')}} @else {{$product->gambar}} @endif">
								@error('picture')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror
								<br>
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
										</ul>
									</div>
								@endif
								<button type="submit" class="btn btn-primary" style="margin-top: 20px;">Edit Category</button>
							</form>
						</div>
					</div>
					<!-- END INPUTS -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection