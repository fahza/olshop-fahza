@extends('../vioAdmin/template/mainAdmin')

@section('title','Products')

@section('content')
<div class="main">
   <div class="main-content">
   @if( Session::has("success"))
      <div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <i class="fa fa-check-circle"></i> {{Session::get('success')}}
      </div>
   @endif
      <div class="container-fluid">
         <div class="panel-body">
            <table class="table table-hover">
               <thead class="text-center">
                  <tr>
                     <th>No</th>
                     <th>Category ID</th>
                     <th>Image</th>
                     <th>Name</th>
                     <th>Stock</th>
                     <th>Price</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($product as $pro)
                  <tr>
                     <td class="text-center">{{$loop->iteration}}</td>
                     <td class="text-center">{{$pro->category_id}}</td>
                     <td class="text-center">{{$pro->gambar}}</td>
                     <td class="text-center">{{$pro->nama}}</td>
                     <td class="text-center">{{$pro->stok}}</td>
                     <td class="text-center">{{$pro->harga}}</td>
                     <td class="text-center">
                        <a class="btn btn-primary" href="/dashboard/productadmin/edit/{{$pro->id}}">Edit</a> | <a href="{{url('dashboard/productadmin/delete/')}}/{{$pro->id}}" class="btn btn-danger deleteButton" onclick="return confirm('Are you sure ?')">Delete</a>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
            {{$product->links()}}
         </div>
      </div>
   </div>
</div>
@endsection