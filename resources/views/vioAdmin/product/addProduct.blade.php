@extends('vioAdmin/template/mainAdmin')

@section('title','Add New Product')

@section('content')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<div class="panel">
						<div class="panel-heading">
							<div>
								<h3 class="panel-title">New Product</h3>
							</div>
						</div>
						<div class="panel-body">
							<form action="" method="POST">
								@csrf
								<div class="form-group">
									<select name="categoryid" class="form-control @error('categoryid') is-invalid invalid @enderror">
									<option value="">Select Category</option>
									@foreach($categoryID as $id)
									<option value="{{$id->id}}">{{$id->categori}}</option>
									@endforeach
									</select>
								</div>
								<div class="form-group">
									<input type="text" name="name" class="form-control @error('name') is-invalid invalid @enderror" placeholder="Name" value="{{old('name')}}">
									@error('name')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>		
								<div class="form-group">
									<input type="text" name="warna" class="form-control @error('warna') is-invalid invalid @enderror" placeholder="Color" value="{{old('warna')}}">
									@error('warna')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>
								<div class="form-group">
									<input type="text" name="ukuran" class="form-control @error('ukuran') is-invalid invalid @enderror" placeholder="Size" value="{{old('ukuran')}}">
									@error('ukuran')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>					
								<div class="form-group">
									<input type="text" name="stock" class="form-control @error('stock') is-invalid invalid @enderror" placeholder="Stock" value="{{old('stock')}}">
									@error('stock')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>			
								<div class="form-group">
									<input type="text" name="price" class="form-control @error('price') is-invalid invalid @enderror" placeholder="Price" value="{{old('price')}}">
									@error('price')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>
								<div class="form-group">
									<label for="picture">Picture</label>
								    <input type="file" name="picture" class="form-control-file @error('picture') is-invalid invalid @enderror" id="picture" value="{{old('picture')}}">
								    @error('picture')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>
								<button type="submit" class="btn btn-primary" style="margin-top: 20px;">Add New Product</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection