@extends('../vioAdmin/template/mainAdmin')

@section('title','Categories')

@section('content')
<div class="main">
   <div class="main-content">
   @if( Session::has("success"))
      <div class="alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <i class="fa fa-check-circle"></i> {{Session::get('success')}}
      </div>
   @endif
      <div class="container-fluid">
         <div class="panel-body">
            <table class="table table-hover">
               <thead class="text-center">
                  <tr>
                     <th>No</th>
                     <th>Name</th>
                     <th>Urutan</th>
                     <th>Aksi</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($categories as $category)
                  <tr>
                     <td class="text-center">{{$loop->iteration}}</td>
                     <td class="text-center">{{$category->categori}}</td>
                     <td class="text-center">{{$category->urutan}}</td>
                     <td class="text-center">
                        <a class="btn btn-primary" href="/dashboard/categories/edit/{{$category->id}}">Edit</a> | <a href="{{url('dashboard/categories/delete/')}}/{{$category->id}}" class="btn btn-danger deleteButton" onclick="return confirm('Are you sure ?')">Delete</a>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
            {{$categories->links()}}
         </div>
      </div>
   </div>
</div>
@endsection