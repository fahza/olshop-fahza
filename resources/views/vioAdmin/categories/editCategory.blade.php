@extends('../vioAdmin/template/mainAdmin')

@section('title','Edit Categories')

@section('content')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<!-- INPUTS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Edit Category</h3>
						</div>
						<div class="panel-body">
							<form action="" method="POST">
								@csrf 
								<input type="text" class="form-control @error('category') invalid is-invalid @enderror" name="category" placeholder="Category" value="@if(old('category')) {{old('category')}} @else {{$category->categori}} @endif">
								@error('category')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror
								<br>
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
										</ul>
									</div>
								@endif
								<button type="submit" class="btn btn-primary" style="margin-top: 20px;">Edit Category</button>
							</form>
						</div>
					</div>
					<!-- END INPUTS -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection