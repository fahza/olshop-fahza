<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        // $categori = \App\category::find(2);
        // dd($categori->product);
        // ngetes relasi 
        $products = Product::all();

        return view('home', compact('products'));
    }

    public function contact() {
        return view('contact');
    }
}
