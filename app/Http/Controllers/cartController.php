<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Auth;
use DB; 
use App\User ; 

class cartController extends Controller
{
    public function getCart()
    {
        if (Auth::id()==null) {
            $gagal =["pesan"=>"anda belum login", "status"=>0];
            return response()->json($gagal);
        }

        DB::enableQueryLog();
        $cart = Cart::where("user_id",Auth::id())->orderBy("id","DESC")->get();
        
        foreach($cart as $row) {
            $list[] = [
                        "id"=>$row->id,
                        "nama"=>$row->getProduct->nama, 
                        "harga"=>$row->getProduct->harga,
                        "gambar"=>$row->getProduct->gambar, 
                        "jmlh"=>$row->qty
                    ];
               
        };

        // $berhasil = ["pesan"=>"berhasil", "status"=>1, "user_id"=>Auth::id()];

        $berhasil['pesan'] = "berhasil"; 
        $berhasil['status'] = 1 ; 
        $berhasil['user_id'] = Auth::id() ; 
        $berhasil["list"] = $list;
         
        return response()->json($berhasil);
    }

    public function postCart(Request $request){
        // cek produk sdh pernah dipilih/blm
        $cek = Cart::where([
            ["user_id",Auth::id()],
            ["product_id", $request->product]
        ])->count();
        
        $cart = Cart::where([
            ["user_id",Auth::id()],
            ["product_id", $request->product]
        ])->first();

        if ($cek == 1) {
            $cart->qty = $cart->qty + 1;
            $cart->save();
        } else {
            $newCart = new Cart;

            $newCart->user_id = Auth::id();
            $newCart->product_id = $request->product;
            $newCart->qty = 1;
            $newCart->save();
        }
    }

    public function cartDel(Request $request){
        // $cart = Cart::find($request->delete);
        Cart::find($request->delete)->delete();
    }

    public function plusMin(Request $request){
        $cart = ($request->tmbh);
        $min = ($request->min);
        $cek = Cart::find($request->id);
        if ($cart=="plus") {
            $cek->qty = $cek->qty + 1;
            $cek->save();
        } elseif ($min == "min") {
            if ($cek->qty > 1) {
                $cek->qty = $cek->qty - 1;
                $cek->save();
            } elseif ($cek->qty = 1) {
                $cek->delete();
            }
        }
    }
}
