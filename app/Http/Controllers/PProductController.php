<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PProductController extends Controller
{
    public function product(){
        return view('product');
    }

    public function detail($id){
        return view('detail');
    }
}
