<?php

namespace App\Http\Controllers\adminControl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Session;

class categoryController extends Controller
{
    
    public function addCategory() {
        return view('vioAdmin/categories/addCategory');
    }
    public function index(Request $request){
        $categories = category::paginate(5);
        return view('vioAdmin.categories.categories', compact('categories'));
    }
    public function add(Request $request) {

        $this->validate($request, [
            'category'=>'required|alpha'
            ]);
        
        //get max + 1
        $jmlh = Category::max('urutan') +1;
        
        //save ke database
        $category = new category;
        $category->categori = $request->category;
        $category->urutan = $jmlh;
        $category->save();
        Session::flash('success','Data berhasil di tambahkan :)');

        //berhasil
        return redirect('dashboard/categories');
    }

    public function destroy($id){
        $category = category::find($id);
        category::find($id)->delete();

        session::flash('success', 'Data berhasil di hapus');
        return redirect('dashboard/categories');  
    }
    
    public function edit($id){
        $category = category::find($id);
        return view('vioAdmin.categories.editCategory', compact('category'));
    }

    public function update(Request $request, $id) {
        // validasi
        $this->validate($request, [
            'category'=>'required|alpha'
        ]);

        $category = category::find($id);
        $category->categori = $request->category;
        $category->save();

        session::flash('success', 'Data telah diubah :) ');
        return redirect('dashboard/categories');
    }
}
