<?php

namespace App\Http\Controllers\adminControl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cart;
use Auth;
use App\order;
use App\order_detail;
use App\User ; 

class orderController extends Controller
{
    public function order() {
        DB::table('orders')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->get();
    }

    public function orderDetail() {
        DB::table('order_details')
            ->join('orders', 'orders.id', '=', 'order_details.order_id')
            ->get();
    }

    public function postOrder(Request $request){
        $newOrder = new order;
        $orderDetail = new order_detail;

        $nama = ($request->namaPenerima);
        $alamat = ($request->alamat);
        $contact = ($request->contact);
        $total = ($request->total);

        $newOrder->user_id = Auth::id();
        $newOrder->alamat = $alamat;
        $newOrder->contact = $contact;
        $newOrder->penerima = $nama;
        $newOrder->total = $total;
        $newOrder->save();


        $del = $dataCart = Cart::where("user_id",Auth::id()); 
       
        $dataCart->get();
        foreach($dataCart as $data){
            $orderDetail->order_id = $newOrder->id; 
            $orderDetail->product_id = $data->product_id;
            $orderDetail->order_qty = $data->qty;
            $orderDetail->harga = $data->getProduct->harga;
            $orderDetail->subtotal = $data->qty * $data->getProduct->harga;
            $orderDetail->save();
        }
        $del->delete() ;
        
    }
}
