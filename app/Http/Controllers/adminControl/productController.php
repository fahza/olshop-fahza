<?php

namespace App\Http\Controllers\adminControl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class productController extends Controller
{
    public function addProduct() {
        $categoryID = category::all();
        return view('vioAdmin.product.addProduct', compact('categoryID'));
    }

    public function index(Request $request){
        $product = Product::paginate(5);
        return view('vioAdmin.product.productAdmin', compact('product'));
    }

    public function add(Request $request){
        // validasi
        $this->validate($request,[
            'categoryid'=>'required|integer',
            'name'      =>'required|string',
            'stock'     =>'required|integer',
            'price'     =>'required|integer',
            'picture'   =>'required|string',
            'warna'     =>'required|string',
            'ukuran'    =>'required|string'
        ]);

        $jmlh = Product::max('kode') +1;

        $product = new Product;
        $product->category_id = $request->categoryid;
        $product->nama = $request->name;
        $product->stok = $request->stock;
        $product->harga = $request->price;
        $product->gambar = $request->picture;
        $product->warna = $request->warna;
        $product->ukuran = $request->ukuran;
        $product->kode = $jmlh;
        $product->save();

        return redirect('dashboard/productadmin');
    }

    public function destroy($id){
        $product = Product::find($id);
        Product::find($id)->delete();

        return redirect('dashboard/productadmin');
    }

    public function edit($id){
        $categoryID = category::all();
        $product = Product::find($id);

        return view('vioAdmin.product.editProduct', compact('product', 'categoryID'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'categoryid'=>'required|integer',
            'name'      =>'required|string',
            'stock'     =>'required|integer',
            'price'     =>'required|integer',
            'picture'   =>'required|string',
            'warna'     =>'required|string',
            'ukuran'    =>'required|string'
        ]);
        $product = Product::find($id);

        $product->category_id = $request->categoryid;
        $product->nama = $request->name;
        $product->stok = $request->stock;
        $product->harga = $request->price;
        $product->gambar = $request->picture;
        $product->warna = $request->warna;
        $product->ukuran = $request->ukuran;

        $product->save();

        return redirect('dashboard/productadmin');
    }
}
