<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function product(){
        return $this->hasOne('App\Product','id');
    }

    public function getProduct(){
        return $this->hasOne('App\Product','id','product_id');
    }

    
}
