<?php
use App\Http\Middleware\CheckAdmin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// violet
route::get('/', 'HomeController@home');
route::get('/contact', 'HomeController@contact');
route::get('/product', 'PProductController@product');
route::get('/product/{id}','PProductController@detail');
route::get('/chart', 'payController@chart');

// cart
route::get('cart', 'cartController@getCart');
route::post('cart','cartController@postCart');
route::post('cart/delete/{id}','cartController@cartDel');
route::post('cart/plusMin/{id}','cartController@plusMin');

route::get('/check', 'payController@check');


Route::prefix('dashboard')->middleware(CheckAdmin::class)->group(function() {
   Route::get('/','adminControl\adminController@homeAdmin' ); 
   Route::get('/categories', 'adminControl\categoryController@index' );
   Route::get('/productadmin','adminControl\productController@index');
   
   Route::get('/order', 'adminControl\orderController@order');
   Route::post('/order', 'adminControl\orderController@postOrder');
   Route::get('/orderdetail', 'adminControl\orderController@orderDetail');

   Route::get('/addproductadmin','adminControl\productController@addProduct');
   Route::post('/addproductadmin','adminControl\productController@add');
   Route::get('/productadmin/delete/{id}', 'adminControl\productController@destroy');
   Route::get('/productadmin/edit/{id}', 'adminControl\productController@edit');
   Route::post('/productadmin/edit/{id}', 'adminControl\productController@update');

   Route::get('/addcategory','adminControl\categoryController@addCategory');
   Route::post('/addcategory','adminControl\categoryController@add');
   Route::get('/categories/delete/{id}', 'adminControl\categoryController@destroy');
   Route::get('/categories/edit/{id}', 'adminControl\categoryController@edit');
   Route::post('/categories/edit/{id}', 'adminControl\categoryController@update');

});



Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');