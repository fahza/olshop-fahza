<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            userSeeder::class,
            categoriesTableSeeder::class,
            productsTableSeeder::class,
            cartSeeder::class,
            orderSeeder::class,
            orderDetailSeeder::class
        ]);
    }
}
