<?php

use Illuminate\Database\Seeder;

class productsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'gambar' => 'img-1.jpg',
            'nama' => 'Kaos',
            'kode' => 112233,
            'warna' => 'Merah',
            'ukuran' => 'XL',
            'stok' => 5,
            'harga' => 125000,
            'category_id' => '1'
        ]);

        DB::table('products')->insert([
            'gambar' => 'img-2.jpg',
            'nama' => 'baju',
            'kode' => 112233,
            'warna' => 'Merah',
            'ukuran' => 'XL',
            'stok' => 5,
            'harga' => 125000,
            'category_id' => '2'
        ]);

        DB::table('products')->insert([
            'gambar' => 'img-3.jpg',
            'nama' => 'kemeja',
            'kode' => 112233,
            'warna' => 'Merah',
            'ukuran' => 'XL',
            'stok' => 5,
            'harga' => 125000,
            'category_id' => '3'
        ]);
    }
}
