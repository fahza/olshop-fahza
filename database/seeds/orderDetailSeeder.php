<?php

use Illuminate\Database\Seeder;

class orderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_details')->insert([
            'order_id'=>1,
            'product_id'=>1,
            'order_qty'=>2,
            'harga'=>150000,
            'subtotal'=>300000
        ]);

        DB::table('order_details')->insert([
            'order_id'=>2,
            'product_id'=>1,
            'order_qty'=>2,
            'harga'=>150000,
            'subtotal'=>300000
        ]);
    }
}
