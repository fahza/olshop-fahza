<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => "Fahza",
                'email' => "admin@gmail.com",
                'password' => bcrypt('123456'),
                'level' => 'admin'
            ]
        );

        DB::table('users')->insert(
            [
                'name' => "Dodi",
                'email' => "dodi@gmail.com",
                'password' => bcrypt('123456'),
                'level' => 'member'
            ]
        );
    }
}
