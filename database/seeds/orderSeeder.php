<?php

use Illuminate\Database\Seeder;

class orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'user_id'=>1,
            'alamat'=>'JL.D.Sentani Raya, Kota Malang',
            'contact'=> '08123456789',
            'resi'=>'12345678900987654321',
            'kurir'=>'J&T',
            'penerima'=>'Fahza Pergata',
            'total'=>5000000,
            'status'=>'Succes'
        ]);

        DB::table('orders')->insert([
            'user_id'=>2,
            'alamat'=>'JL.D.Sentani Raya, Kota Malang',
            'contact'=> '08123456789',
            'resi'=>'12345678900987654321',
            'kurir'=>'J&T',
            'penerima'=>'Fahza Pergata',
            'total'=>5000000,
            'status'=>'Succes'
        ]);
    }
}
