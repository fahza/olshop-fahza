<?php

use Illuminate\Database\Seeder;

class categoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'categori' => 'Baju',
            'urutan' => 1
        ]);

        DB::table('categories')->insert([
            'categori' => 'Kemeja',
            'urutan' => 1
        ]);

        DB::table('categories')->insert([
            'categori' => 'Kaos',
            'urutan' => 1
        ]);
    }
}
